
using RelevanceStacktrace
println("--------------------------------------------------------------------------------")

#%%
using Revise

includet("../src/DataPipe.jl")

using .DataPipe.DataWorld: io_type
using .DataPipe: data_pipe


ctx = (datatype=io_type[:crypto],
      data_name = "crypto_data/crypto.40d_5m_7m.jld2", 
      train_ratio = 1.0, data_graph_name = "crypto_graph/initial.-1.4.json")
@time data = data_pipe(ctx.datatype, ctx.data_name, train_ratio=ctx.train_ratio)





#%%
using .DataPipe: runids_folder_from_to

println("--------------------------------------------------------------------------------")
## With RUN_ID
from_format = :JSON
from_storage = :FILE
# from_storage = :REDIS

to_format = :JLD2
to_storage = :FILE
runids_folder_from_to(from_format, from_storage, to_format, to_storage)

####
# d_name = "mnist_data/mnist_test.0"
# @time dataset = io_pipe(d_name * ".json", train_ratio = 1.0)
# @time dataset = io_pipe(d_name * ".json", train_ratio = 1.0)
# @time dataset = io_pipe(d_name * ".jld2", train_ratio = 1.0)
# @time dataset = io_pipe(d_name * ".jld2", train_ratio = 1.0)
####

for ctx in [
  # (data_name = "raw_data/raw_data.0.json", train_ratio = 1.0, data_graph_name = "raw_graph/0.1.2/raw_graph.0.json"),
  # (data_name = "raw_data/raw_data.2.json", train_ratio = 1.0, data_graph_name = "raw_graph/0.1.2/raw_graph.0.json"),
  # (data_name = "raw_data/raw_data.3.json", train_ratio = 1.0, data_graph_name = "raw_graph/0.1.3/raw_graph.0.json"),
  # (data_name = "raw_data/raw_data.4.json", train_ratio = 1.0, data_graph_name = "raw_graph/0.1.4/raw_graph.0.json"),
  # (data_name = "diabtrend_data/data.1.json", train_ratio = 1.0, data_graph_name = "raw_graph/0.1.5/raw_graph.0.json"),
  (data_name = "crypto_data/crypto.0.json", train_ratio = 1.0, data_graph_name = "crypto_graph/initial.0.json"),
  # (data_name = "mnist_data/mnist_test.0.json", train_ratio = 1.0, data_graph_name = "mnist_graph/graph.0.json"),
  # (data_name = "mnist_data/mnist_train.0.json", train_ratio = 1.0, data_graph_name = "mnist_graph/graph.0.json"),
  # (data_name = "numer_ai/unconverted.json", train_ratio = 1.0,data_graph_name = "numer_ai/numerai_graph.0.json"),
]
println("-------")
  @time dataset = io_pipe(ctx.data_name, train_ratio = ctx.train_ratio)
  X, Y, T_C = dataset.d.ins, dataset.d.outs, dataset.d.time_connect
  @show typeof(X)
  @show typeof(Y)
  @show typeof(T_C)
  @sizes (X)
  X, Y, T_C = train_batch(dataset)
  X, Y, T_C = train_batch(dataset)
  @sizes (X)
  # fname_split = "_1_datasets/"*join(split(ctx.data_name,".")[1:end-1],".") * ".jld2"
  # @show fname_split
  # @time FileIO.save(fname_split, Dict("dataset" => dataset))
  # @time FileIO.load(fname_split, "dataset")
end

