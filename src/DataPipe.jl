
module DataPipe

using Revise

include("Utils.jl")  # TODO includet!!!!! somehow??? Why doesn't is work... :()
include("DataWorld.jl")


using .DataWorld: DiabTrendData, CryptoData, CoinInfo, ConcreteDataSet, DataFixed, DataSetFixed

using .Utils: get_dims, len, to_ranges, select  # imported from utils
using Storage: loader, path_splitter, filename_splitter, parse_extension

using Random
using DataStructures
using EllipsisNotation
using InteractiveUtils
using Distributions
using JLD2  # FileIO imports it... 
using FileIO  # JLD2 imports
using Glob

using Boilerplate: @sizes, @typeof, @display

get_last_idx(d::CryptoData) = begin
  T = size(d.ins[1], 1)
  @assert all(length(d.timestamps[1]) .== length.(d.timestamps)) "Not same timestamps!!"
  get_last_idx(d.timestamps[1], T)
end
get_last_idx(ts::Vector{Float64}, T::Int) = begin
  allts = length(ts) - 1
  (allts ÷ T +1, (allts % T) + 1) # Batch, Time
end

function slice_list_of_arrs(slic::Union{UnitRange{Int64},Vector{Int64}}, list_of_arr::Tuple{A,B,C,D,E}) where {A,B,C,D,E}
  ([reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[1]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[2]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[3]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[4]],
   [time_slice[slic] for time_slice in list_of_arr[5]],  # ONLY Crypto. Train/validation ratio... 
   )
end
function slice_list_of_arrs(slic::Union{UnitRange{Int64},Vector{Int64}}, list_of_arr::Tuple{A,B,C,D}) where {A,B,C,D}
  ([reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[1]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[2]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[3]],
   [time_slice[slic] for time_slice in list_of_arr[4]])  # ONLY Crypto. Train/validation ratio... 
end
function slice_list_of_arrs(slic::Union{UnitRange{Int64},Vector{Int64}}, list_of_arr::Tuple{A,B,C}) where {A,B,C}
  ([reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[1]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[2]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[3]])
end
function slice_list_of_arrs(slic::Union{UnitRange{Int64},Vector{Int64}}, list_of_arr::Tuple{A,B}) where {A,B}
  ([reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[1]],
   [reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[2]])
end
function slice_list_of_arrs(slic::Union{UnitRange{Int64},Vector{Int64}}, list_of_arr::Tuple{A}) where A
  ([reduce(vcat,time_slice[slic]) for time_slice in list_of_arr[1]],)
end

transpose_data(Ys, ::DiabTrendData) = ([Ys[1][t][.., 1:1] for t = 1:60], [Ys[1][t][.., 2:2] for t = 1:60])  # DIABTREND Data! Time_steps == 60 (precompile return type optimisation with dispatch)
transpose_data(Ys, ::Any) = Ys
function batch(data, req_batch_size) # TODO train-valid separation at higher level
  # @typeof data.ins
  data_batch_size = size(data.ins[1][1], 1)::Int
  if data_batch_size == 0
    (@assert "No data in set")::AssertionError
  end
  X, Y, TC = data.ins, data.outs, data.time_connect #, data.valid
  if req_batch_size == 0
    req_batch_size = data_batch_size
  end
  N = req_batch_size / data_batch_size |> ceil |> Integer
  indices = sort(Random.shuffle(repeat(collect(1:data_batch_size), N))[1:req_batch_size])
  Xs = slice_list_of_arrs(indices, X)
  Ys = slice_list_of_arrs(indices, Y)

  # TCC = isempty(TC[1]) || isempty(TC[1][1]) ? TC : slice_list_of_arrs(indices, TC)
  #V = V === nothing ? V : slice_list_of_arrs(indices, V...)
  if data.time_dim
    timesteps=length(Ys[1])::Int64
    Yss =transpose_data(Ys, data)
  else
    Yss = Ys
  end
  if data.time_dim ==false
    TCC = TC 
  else
    if eltype(TC[1]) === Any # It means it is NetworkData
      TCC1::Tuple{Array{Float32,1}} = slice_list_of_arrs(indices, TC)
      return Xs, Yss, TCC1
    else
      TCC2::Tuple{Array{Array{Float32,3},1}} = slice_list_of_arrs(indices, TC)
      return Xs, Yss, TCC2
    end
  end
  # return Yss 
  # @typeof (Xs, Yss, TCC)
  # return (Xs, Yss, TCC)::Tuple{Tuple{Array{Array{Float32,6},1},Array{Array{Int64,2},1},Array{Array{Int64,2},1}},Tuple{Array{Array{Float32,2},1}},Tuple{Array{Float32,1}}}
  return Xs, Yss, TCC
end
function train_batch(dataset::ConcreteDataSet{T}, req_batch_size::Int = 0) where T
  # @code_warntype batch(dataset.d, req_batch_size, )
  return batch(dataset.d, req_batch_size, ) 
end
function train_batch(dataset::DataSetFixed, req_batch_size::Int = 0)
  # @code_warntype batch(dataset.d, req_batch_size, )
  return batch(dataset.d, req_batch_size, ) 
end
function valid_batch(dataset::DataSetFixed, req_batch_size::Int = 0)
  return batch(dataset.d_val, req_batch_size, )
end

@inline operatrion_x12_x12(data, raw, x, ir, jr) = for i in ir, j in jr
	data[x][i, j] = raw[x][i][j]
end
# @inline operatrion_x12_2x1(data,raw,x, ir,jr) = for i=ir, j=jr data[j][x][i]=raw[x][i][j] end
# @inline operatrion_123x4_23x41(data,raw,x, ir,jr,kr,lr) = (for i=ir, j=jr, k=kr, l=lr data[j,k][x][l,i]=raw[i][j][k][x][l] end)
@inline operatrion_x123_x123(data, raw, x, ir, jr, kr) = for i in ir, j in jr, k in kr
	data[x][j, k, l] = raw[x][i][j][k]
end

@inline operatrion_x1234_x2341(data, raw, x, ir, jr, kr, lr) = for i in ir, j in jr, k in kr, l in lr
	data[x][j, k, l, i] = raw[x][i][j][k][l]
end
function get_dimensions(separated_input)
  batch_type = ["batch", "batch_", "batch1", "batch2", "batch3"]
  time_type = ["time", "time_", "time1", "time2", "time3"]
  batch = []
  input = []
  time = []
  for (i, dim_name) in enumerate(separated_input[2:end])
    if dim_name[1:min(end, 6)] in batch_type
      push!(batch, i)
    elseif dim_name[1:min(end, 5)] in time_type
      push!(time, i)
    else
      push!(input, i)
    end
  end
  return [batch..., input..., time...]
end
function get_all_dims(desc, data)
  # desc = Any[Any[Any["insulin", "in_data","time"],Any["carbohydrate", "in_data","in_data2","time"]]]
  i_ptr = data
  swap_to, dimensions_from = [], []
  if typeof(desc[1]) == String
    desc = [desc]
  end
  for (i, separated_input) in enumerate(desc)
    push!(swap_to, get_dimensions(separated_input))
    push!(dimensions_from, get_dims(i_ptr[i]))
  end
  swap_from = [[i for i = 1:length(swapy)] for swapy in swap_to]
  dimensions_to = [dimy[swapy] for (swapy, dimy) in zip(swap_to, dimensions_from)]
  return swap_from, dimensions_from, swap_to, dimensions_to
end

function assign!(data, raw, from, from_dim, to, to_dim)
  order_fns = Dict(([1, 2], [1, 2]) => operatrion_x12_x12, ([1, 2, 3], [1, 2, 3]) => operatrion_x1234_x2341, ([1, 2, 3, 4], [2, 3, 4, 1]) => operatrion_x1234_x2341)

  for x in len(from_dim)
    fr_range = to_ranges(from_dim[x])
    our_fn = order_fns[from[x], to[x]]
    our_fn(data, raw, x, fr_range...)
  end
end
function rearrange_raw(desc, raw)
  swap_from, dim_from, swap_to, dim_to = get_all_dims(desc, raw)
  data = similar(dim_to)
  assign!(data, raw, swap_from, dim_from, swap_to, dim_to)
  data
end
function parse_crypto(i_data, market_names; normalize=true, tradeinfuture=false, verbose=true, idx=nothing)
  B, M, F, T = size(i_data)
  @assert F == 5 "Only 5 inputs mode is supported."
  # normalise by the 
  indices, btc_index, coinmap = get_market_indices(market_names, B)
  verbose && @show keys(coinmap)
  if normalize
    splitted_markets = [split(m, "/") for m in market_names]
    for (i, m) in enumerate(splitted_markets)
      if coinmap[m[1]].mean === nothing # TODO only make calc on BTC!
        coinmap[m[1]].mean = mean(i_data[end-1, i, 1, :]) * coinmap[m[2]].mean
      end
      if coinmap[m[2]].mean === nothing
        coinmap[m[2]].mean = coinmap[m[1]].mean / mean(i_data[end-1, i, 1, :]) 
      end
    end
    for (i, m) in enumerate(splitted_markets)
      i_data[:, i, 1:4, :] .= i_data[:, i, 1:4, :] .* (coinmap[m[2]].mean / coinmap[m[1]].mean)
    end
  end
  i_data[:, :, 5:5, :] .= i_data[:, :, 5:5, :] ./ mean(i_data[end-1:end-1, :, 5:5, :], dims = [4]) # volumen don't need superhigh normalization precision
  # i_data[:, :, 1:4, :] .= i_data[:, :, 1:4, :] ./ mean_coins_B1
  # i_data = i_data ./ mean_coins_B1
  i_data = Float32.(i_data)

  o_data = similar(i_data)
  if tradeinfuture
    o_data[.., 1:T-1] .= i_data[.., 2:T]
    o_data[1:B-1, .., T] .= i_data[2:B, .., 1]
    o_data[B, .., T] .= 0.0f0
  else
    o_data .= i_data
  end

  i_data = [[i_data[i:i, .., t] for i = 1:B] for t = 1:T]
  o_data = [[o_data[i:i, .., t] for i = 1:B] for t = 1:T]
  C = length(coinmap)
  prices_C = o_data .|> d -> d .|> get_price_C(indices, C)
  prices_A = o_data .|> d -> d .|> get_price_A(indices, C)
  # TODO I think amount * price_A should be relative to price_C
  if verbose
    @sizes prices_C
    @display prices_C[2][8][1,:,:]
    @display prices_C[72][8][1,:,:]
    @display prices_A[3][2][1,:,:]
  end
  coin_value_inbtc = o_data .|> d -> get_coin_values_inbtc(indices, btc_index, C).(d) 
  if verbose
    @sizes coin_value_inbtc
    @display coin_value_inbtc[2][8][1,:]
    @display coin_value_inbtc[72][8][1,:]
  end
  val_ratio = 50
  VAL_CHANCE = 0.4  # 40% chance to be validation
  y_val = [B==i && idx !== nothing ? [0, idx[2]] : [0, i<B-1 && rand()<VAL_CHANCE ? val_ratio : T] for i in 1:B]
  o_data = Tuple([o_data, prices_C, prices_A, coin_value_inbtc, [y_val]])
  t_c = [y_val[i][2]==T ? ones(Float32, 1, M, 1) : zeros(Float32, 1, M, 1) for i in 1:B] # TODO finish stateconnet
  (Tuple([i_data]), o_data, ([t_c],)), coinmap
end
function data_from_dict(raw)
  # @show get_dims(raw.ins)
  time_dim = false
  if hasproperty(raw, :ins_shape) || hasproperty(raw, :outs_shape) ## crypto
    i_data = Float32.(raw.ins)
    i_data = hasproperty(raw, :ins_shape) ? reshape(i_data, raw.ins_shape...) : i_data  # TODO data shape is hardcoded 1,dims...
    i_data = i_data[:, :, 2:end, :]
    (i_data, o_data, t_c), _ = parse_crypto(i_data, split(raw.in_description[4], "_")[2:end])
    time_dim = true
  else
    i_data = rearrange_raw(raw.in_description, raw.ins)
    o_data = rearrange_raw(raw.out_description, raw.outs)
    data_batch_size = size(i_data[1], 1)
    if length(i_data[1]) > 0 && length(size(i_data[1])) > 3 && size(i_data[1],4) == 60 # DiabTrend Data
      time_size=size(i_data[1],4)
      i_data = Tuple([[[i_data[1, ..][i:i, ..,t] for i = 1:data_batch_size] for t = 1:time_size]])
      o_data = Tuple([[[o_data[1, ..][i:i, ..,t] for i = 1:data_batch_size] for t = 1:time_size]])
      time_dim = true
      t_c = (rearrange_raw(raw.time_connect_description, raw.state_connect),)
      # t_c = ([[t_c[1][1][i,j,..] for i in 1:size(t_c[1][1],1), j in 1:size(t_c[1][1],2)]],) # CORRECT BATCH handling
      t_c = ([[reshape(t_c[1][1][i,..], 1, :, 1) for i in 1:size(t_c[1][1],1)]],) # TODO why we need reshape?
    else
      i_data = Tuple([[[i_data[1, ..][i:i, ..]|>reshape(1, 1, :)  for i = 1:data_batch_size]]])
      o_data = Tuple([[[o_data[1, ..][i:i, ..]|>reshape(1, 1, :) for i = 1:data_batch_size]]])
      t_c = [[]]  # TODO move BATCH  based slicing 
    end
  end
  Data(t_c, i_data, o_data, time_dim)
end
HardcodeTypes(d_dyn) = (d_t=DataFixed{typeof(d_dyn.d.time_connect),typeof(d_dyn.d.ins),typeof(d_dyn.d.outs)}(d_dyn.d.time_connect,d_dyn.d.ins,d_dyn.d.outs,d_dyn.d.time_dim);
                        d_v=DataFixed{typeof(d_dyn.d_val.time_connect),typeof(d_dyn.d_val.ins),typeof(d_dyn.d_val.outs)}(d_dyn.d_val.time_connect,d_dyn.d_val.ins,d_dyn.d_val.outs,d_dyn.d_val.time_dim);
                        DataSetFixed(d_t,d_v,d_dyn.train_ratio))
function HardcodeTypes(ConcreteType, d_dyn)
  # @typeof d_dyn.d.time_connect
  # @typeof d_dyn.d.ins
  # @typeof d_dyn.d.outs
  # @code_warntype fn(Tuple{Vector{Vector{Array{Float32, 3}}}}(d_dyn_hard.d.ins))

  d_t = ConcreteType(time_connect=d_dyn.d.time_connect, ins=d_dyn.d.ins, outs=d_dyn.d.outs, time_dim=d_dyn.d.time_dim)
  d_v = ConcreteType(time_connect=d_dyn.d_val.time_connect, ins=d_dyn.d_val.ins, outs=d_dyn.d_val.outs, time_dim=d_dyn.d_val.time_dim)
  ConcreteDataSet(d_t, d_v, d_dyn.train_ratio)
end
function data_pipe(file_name::String; storage_type::Symbol = :FILE, train_ratio::Float64 = 1.0, prefix="./_1_datasets/")
  path = prefix* file_name
  baseurl, url, data_file = path_splitter(path)
  file = filename_splitter(data_file)
  if parse_extension(file.extension) == :JLD2
    print("$path is ")
    fixed = jldopen(path, "r") do file
      file["dataset"]
    end
    println("loaded!")
    
    return HardcodeTypes(fixed)
  else
    save_fn, load_fn, data_name = loader(path, storage_type)
    data_fixed = data_from_dict(load_fn("IO"))
    # (() -> save_fn("IO", data))
    return HardcodeTypes(DataSet(d = data_fixed, d_val = Data((), (), (), data_fixed.time_dim), train_ratio = train_ratio))
  end
end
function data_pipe(ConcreteType::DataType, file_name::String; storage_type = :FILE, train_ratio::Float64 = 1.0, prefix="./_1_datasets/")
  return HardcodeTypes(ConcreteType, data_pipe(file_name, storage_type=storage_type, train_ratio=train_ratio, prefix=prefix))
end
non_nan_reciproc(v::Float32)::Float32 = v > 1.0f-25 ? 1 / v : 0f0
non_nan_reciproc(v::Float64)::Float64 = v > 1.0e-25 ? 1 / v : 0e0
function get_coin_values_inbtc(indices, btc_index, C)
  function fn(prices)
    values = zeros(Float32, size(prices, 1), C)
    for (j, idx) in enumerate(indices)
      if idx[1] == btc_index
        values[.., idx[2]] .= non_nan_reciproc.(prices[.., j, 4])
      elseif idx[2] == btc_index
        values[.., idx[1]] .= prices[.., j, 4]
      end
    end
    values[.., btc_index] .= select.(prices[.., 1, 4].>0f0, 1f0, 0f0)
    values::Array{Float32,2}
  end
end
function get_price_C(indices, C)
  function fn(prices)
    prices_M = zeros(Float32, size(prices, 1), C, C)
    for (j, idx) in enumerate(indices)
      prices_M[.., idx[2], idx[1]] .= prices[.., j, 4]
      prices_M[.., idx[1], idx[2]] .= non_nan_reciproc.(prices[.., j, 4])
    end
    for i = 1:C
      prices_M[.., i, i] .= 1.0f0
    end
    prices_M::Array{Float32,3}
  end
end
function get_price_A(indices, C)
  function fn(prices)
    prices_A = zeros(Float32, size(prices, 1), C, C)
    for (j, idx) in enumerate(indices)
      prices_A[.., idx[2], idx[1]] .= (prices[.., j, 2] .- prices[.., j, 3]) ./ (prices[.., j, 5] .+ 1f-8)
      prices_A[.., idx[1], idx[2]] .= (non_nan_reciproc.(prices[.., j, 3]) .- non_nan_reciproc.(prices[.., j, 2])) ./ 
                                          (prices[.., j, 5] .+ 1f-8) # TODO think it over, but I think it is okay.
      # @assert any(prices_A[.., idx[2], idx[1]] .>= 0f0) "SMALLLERR!!!!"
      # @assert any(prices_A[.., idx[1], idx[2]] .>= 0f0) "SMALLLERR!!!!"
    end
    prices_A::Array{Float32,3}
  end
end
function get_market_indices(market_names, B)
  splitted_markets = [split(m, "/") for m in market_names]
  coin_order = OrderedSet{String}()
  for m in splitted_markets
    push!(coin_order, m[1])
    push!(coin_order, m[2])
  end
  coin_map = OrderedDict(v => CoinInfo(k, nothing) for (k, v) in enumerate(coin_order))
  indices = [(coin_map[m[1]].index, coin_map[m[2]].index) for j = 1:1, m in splitted_markets]
  btc_index = coin_map["BTC"].index
  coin_map["BTC"].mean = 1e0
  # btc_index = coin_map["ETH"]
  indices, btc_index, coin_map
end

function convert_between(;filepath, f_format, f_storage, to_format, to_storage)
  if to_format == ".jld2"
    baseurl, url, data_file = path_splitter(filepath)
    data = data_pipe("$url/" * data_file, train_ratio = 1.0)
  else
    from_save, from_load, _ = loader(filepath, f_storage)
    data = from_load(filepath)
  end
  to_save, to_load, _ = loader(replace(filepath, f_format => to_format), to_storage)
  to_save(filepath, data)
end

function runids_folder_from_to(from_format, from_storage, to_format, to_storage)
  txt_format = Dict(:JSON => ".json", :CLOUD => ".cloud", :JLD2 => ".jld2")
  extension = txt_format[from_format]
  for (path, url, pattern) in [
    ("_1_datasets/", "raw_data/", "*.*$extension"),
    ("_1_datasets/", "diabtrend_data/", "*.*$extension"),
    ("_1_datasets/", "mnist_data/", "*.*$extension"),
    ("_1_datasets/", "crypto_data/", "*.*$extension"),
    # ("_2_graphsets/", "raw_graph/", "*.*$extension"),
    # ("_2_graphsets/", "network_graph/", "*.*$extension"),
    # ("_1_datasets/", "network_data/", "*.*$extension"),  # DIFFERENT DATASET
  ]
    for filepath in glob(pattern, path * url)
      baseurl, url, data_file = path_splitter(filepath)
      file = filename_splitter(data_file)
      # print(file, data_file,version_num,extension)
      convert_between(filepath=filepath, f_format=txt_format[from_format], f_storage=from_storage, to_format=txt_format[to_format], to_storage=to_storage)
    end
  end
end

# @everywhere cutter(dat) = Tuple([Tuple([length(v)>0 ? [1:10, 1:2, :] : [] for v in time_v] for time_v in dat_t ) for dat_t in dat[1:3]])
cutter(::Val{DiabTrendData}) = dat -> Tuple([Tuple([length(v)>0 ? v[1:50, ..] : [] for v in time_v] for time_v in dat_t ) for dat_t in dat[1:3]])
cutter(::Val{CryptoData}) = dat -> Tuple([Tuple([length(v)>0 ? v[1:end-1, ..] : [] for v in time_v] for time_v in dat_t ) for dat_t in dat[1:3]])::Tuple{Tuple{Vector{Array{Float32, 3}}}, Tuple{Vector{Array{Float32, 3}}, Vector{Array{Float32, 3}}, Vector{Array{Float32, 3}}, Vector{Matrix{Float32}}, Vector{Vector{Tuple{Int64, Int64}}}}, Tuple{Vector{Array{Float32, 3}}}}
cutter(dat::Any) = dat



# @assert precompile(data_pipe, (CryptoData, String,))
# @assert precompile(data_pipe, (DiabTrendData, String,))
# @assert precompile(data_pipe, (MNISTData, String,))
# @assert precompile(data_pipe, (RAWData, String,))
# # @assert precompile(data_pipe, (NetworkData, String,))
# # @assert precompile(Tuple{Core.kwftype(typeof(data_pipe)),NamedTuple{(:train_ratio,), Tuple{Float64}},typeof(data_pipe),String})
# @assert precompile(Tuple{typeof(data_pipe),CryptoData,String})
# @assert precompile(Tuple{typeof(data_pipe),DiabTrendData,String})
# @assert precompile(Tuple{typeof(data_pipe),MNISTData,String})
# @assert precompile(Tuple{typeof(data_pipe),RAWData,String})
# @assert precompile(Tuple{typeof(data_pipe),NetworkData,String})

# PRECOMPILE DIRECTIVES
isfile("PrecompilePkg/precompile_directives/precompile_DataPipe.jl") && include("PrecompilePkg/precompile_directives/precompile_DataPipe.jl")

end  # module DataPipe
