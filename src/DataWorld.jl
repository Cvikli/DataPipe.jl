
module DataWorld

using Parameters
using Random
using EllipsisNotation
using DataStructures
using Flux: unsqueeze, stack
using Boilerplate: @sizes, @typeof, @display

using Distributions

using InteractiveUtils


mutable struct CoinInfo
  index::Int
  mean::Union{Nothing, Float64}
end
# "in_dims":["batch",[["input"]],"time"]
# ["batch",["actions","indexes"],"time"]
# "time_axes": {}, "batch_dims": {"0":"samples"}, "input_shapes":["input1":{"0":28,"1":28}],"ins":[]
mutable struct Data # Bd = {Egymástól 100%-ig független batchek} & Bu = {Egymástól függhető batchek}
  time_connect #::NTuple{Array{Float32,B} where B}# whether time slices are connected or not
  ins #::NTuple{Array{TI,BIT} where TI,BIT}  # T=1
  outs #::NTuple{Array{TO,BOT} where TO,BOT} # T=1
  time_dim::Bool
end
mutable struct DataFixed{T,I,O}
  time_connect::T
  ins::I
  outs::O
  time_dim::Bool
end
abstract type InputData end
@with_kw mutable struct CryptoData <: InputData
  time_connect::Tuple{Vector{Vector{Array{Float32, 3}}}}  # Union{Vector{Vector{Vector{Any}}},Tuple{Vector{Any}},}
  ins::Tuple{Vector{Vector{Array{Float32, 3}}}}  # Union{Tuple{Vector{Vector{Any}}, Vector{Vector{Any}}, Vector{Vector{Any}}},}
  # outs::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Matrix{Float32}}}, Vector{Vector{Tuple{Int64, Int64}}}}}
  outs::Tuple{Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Matrix{Float32}}}, Vector{Vector{Vector{Int64}}}}
  time_dim::Bool
  timestamps::Union{Nothing,Vector{Vector{Float64}}}=nothing
  coinmap::Union{Nothing, OrderedDict{String, CoinInfo}}=nothing
end
get_last_idx(d::CryptoData) = begin
  T = size(d.ins[1], 1)
  @assert all(length(d.timestamps[1]) .== length.(d.timestamps)) "Not same timestamps!!"
  get_last_idx(d.timestamps[1], T)
end
get_last_idx(ts::Vector{Float64}, T::Int) = begin
  allts = length(ts) - 1
  (allts ÷ T +1, (allts % T) + 1) # Batch, Time
end
@with_kw mutable struct DiabTrendData <: InputData
  time_connect::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}}}
  ins::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}}}
  outs::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}}}
  time_dim::Bool
end
@with_kw mutable struct MNISTData <: InputData
  time_connect::Union{Tuple,Vector{Vector{Any}}}
  ins::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}}}
  outs::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}}}
  time_dim::Bool
end
@with_kw mutable struct RAWData <: InputData
  time_connect::Union{Tuple,Vector{Vector{Any}}}
  ins::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}}}
  outs::Union{Tuple,Tuple{Vector{Vector{Array{Float32, 3}}}}}
  time_dim::Bool
end
@with_kw mutable struct NetworkData <: InputData
  time_connect
  ins::Tuple{Vector{Vector{Array{Float32, 6}}}, Vector{Vector{Matrix{Int64}}}, Vector{Vector{Matrix{Int64}}}}
  outs::Tuple{Vector{Vector{Matrix{Float32}}}}
  time_dim::Bool
end
const io_type = Dict{Symbol,DataType}(:crypto=> CryptoData,
                                      :diabtrend=> DiabTrendData,
                                      :mnist=> MNISTData,
                                      :test=> RAWData,
                                      :network=> NetworkData
                                      )
Base.length(data::Data) = size(data.ins[1][1], 1)
# Base.length(data::InputData) = size(data.ins[1][1], 1)
Base.length(data::DataFixed) = size(data.ins[1][1], 1)
# get_meta(ex::Data{BIT}) where {BIT} = A,B,C
# get_meta(ex::Array{Tuple{Array},B}) where {B} = B
# get_meta(ex::Array{Tuple{Array{Float32,TMP}},B}) where {B,TMP} = B,1
# get_meta(ex::Array{NTuple{X,Array{Float32,TMP}},B}) where {B,X,TMP} = B,X
@with_kw mutable struct DataSet
  d::Data
  d_val::Data
  train_ratio::Float64 = 1.0
end
@with_kw mutable struct DataSetFixed{T,V}
  d::T
  d_val::V
  train_ratio::Float64 = 1.0
end
@with_kw mutable struct ConcreteDataSet{ConcreteType}
  d::ConcreteType
  d_val::ConcreteType
  train_ratio::Float64 = 1.0
end


get_empty_Data(time_dim=false) = DataFixed(Tuple([[]]), ([[]], [[]], [[]]), ([[]],), time_dim)
get_empty_Data(::CryptoData, time_dim=false) = CryptoData(([[Array{Float32,3}(undef,0,0,0)]],), ([[Array{Float32,3}(undef,0,0,0)]],), ([[Array{Float32,3}(undef,0,0,0)]],[[Array{Float32,3}(undef,0,0,0)]],[[Array{Float32,3}(undef,0,0,0)]],[[Array{Float32,2}(undef,0,0)]],[[Array{Int64,1}(undef,0)]]), time_dim, nothing, nothing)
get_empty_NetworkData(time_dim=false) = NetworkData(([Array{Float32,3}[]],),([Array{Float32,6}[]], [Array{Int64,2}[]],[Array{Int64,2}[]]), ([Array{Float32,2}[]],), time_dim)
function init_dataset( x_new, y_new; train_ratio = 1.0f0)#::Tuple{Array{Int64,2},Array{Int64,2},Array{Float32,2}})
  ConcreteDataSet{NetworkData}(NetworkData(([Array{Float32,3}[]],),
                                            ([x_new[1]],[x_new[2]],[x_new[3]]),
                                            ([y_new[1]],), false),
  get_empty_NetworkData(),train_ratio)
end
# @show init_dataset([randn(Float32,2,2)],[randn(Float32,2,2)]).d
function append!(
  data::DataSetFixed;
  x_new,#::Vector{Array{Float32,BIT} where BIT}, 
  y_new,#::Vector{Array{Float32,BOT} where BOT},  
  time_con,#::Vector{Array{Float32,BOT} where BOT}, 
  save = false,
)
  # write_read_lock.acquire_write()
  if data.d.time_dim
    @assert "append when time is available is unimplemented!"
  end
  is_data_append = data.train_ratio == 1.f0 || 
                   ceil(length(data.d) * (1.f0 - data.train_ratio)) <= ceil(length(data.d_val) * data.train_ratio)
  if (is_data_append)
    append!(data.d; x_new, y_new, time_con)
  else
    append!(data.d_val; x_new, y_new, time_con)
  end
  # data.d.ins=[cat(dims=1, data.d.ins[i], unsqueeze(x_new[i], 1)) for i in 1:length(data.d.ins)]
  # data.d.outs=[cat(dims=1, data.d.outs[i], unsqueeze(y_new[i], 1)) for i in 1:length(data.d.outs)]
  # data.d.time_connect=[cat(dims=1, data.d.time_connect[i], time_con[i]) for i in 1:length(data.d.time_connect)]
  # data.ins = [np.append(*pad_to_same_shapes(data.ins[i], x[np.newaxis], ignore_axis=[0]), axis=0) for i, x in enumerate(x_new)]
  # data.outs = [np.append(data.outs[i], y[np.newaxis], axis=0) for i, y in enumerate(y_new)]
  # write_read_lock.release_write()
end
function append!(
  data::DataFixed;
  x_new,#::Vector{Array{Float32,BIT} where BIT}, 
  y_new,#::Vector{Array{Float32,BOT} where BOT},  
  time_con,#::Vector{Array{Float32,BOT} where BOT}, 
)
  for i = 1:length(data.ins)
    push!(data.ins[i][1], stack(x_new[i], 1))
  end
  for i = 1:length(data.outs)
    push!(data.outs[i][1], stack(y_new[i], 1))
  end
end

function parse_crypto(i_data, market_names; normalize=true, tradeinfuture=false, verbose=true, idx=nothing)
  B, M, F, T = size(i_data)
  @assert F == 5 "Only 5 inputs mode is supported."
  # normalise by the 
  indices, btc_index, coinmap = get_market_indices(market_names, B)
  verbose && @show keys(coinmap)
  if normalize
    splitted_markets = [split(m, "/") for m in market_names]
    for (i, m) in enumerate(splitted_markets)
      if coinmap[m[1]].mean === nothing # TODO only make calc on BTC!
        coinmap[m[1]].mean = mean(i_data[end-1, i, 1, :]) * coinmap[m[2]].mean
      end
      if coinmap[m[2]].mean === nothing
        coinmap[m[2]].mean = coinmap[m[1]].mean / mean(i_data[end-1, i, 1, :]) 
      end
    end
    for (i, m) in enumerate(splitted_markets)
      i_data[:, i, 1:4, :] .= i_data[:, i, 1:4, :] .* (coinmap[m[2]].mean / coinmap[m[1]].mean)
    end
  end
  i_data[:, :, 5:5, :] .= i_data[:, :, 5:5, :] ./ mean(i_data[end-1:end-1, :, 5:5, :], dims = [4]) # volumen don't need superhigh normalization precision
  # i_data[:, :, 1:4, :] .= i_data[:, :, 1:4, :] ./ mean_coins_B1
  # i_data = i_data ./ mean_coins_B1
  i_data = Float32.(i_data)

  o_data = similar(i_data)
  if tradeinfuture
    o_data[.., 1:T-1] .= i_data[.., 2:T]
    o_data[1:B-1, .., T] .= i_data[2:B, .., 1]
    o_data[B, .., T] .= 0.0f0
  else
    o_data .= i_data
  end

  i_data = [[i_data[i:i, .., t] for i = 1:B] for t = 1:T]
  o_data = [[o_data[i:i, .., t] for i = 1:B] for t = 1:T]
  C = length(coinmap)
  prices_C = o_data .|> d -> d .|> get_price_C(indices, C)
  prices_A = o_data .|> d -> d .|> get_price_A(indices, C)
  # TODO I think amount * price_A should be relative to price_C
  if verbose
    @sizes prices_C
    @display prices_C[2][8][1,:,:]
    @display prices_C[72][8][1,:,:]
    @display prices_A[3][2][1,:,:]
  end
  coin_value_inbtc = o_data .|> d -> get_coin_values_inbtc(indices, btc_index, C).(d) 
  if verbose
    @sizes coin_value_inbtc
    @display coin_value_inbtc[2][8][1,:]
    @display coin_value_inbtc[72][8][1,:]
  end
  val_ratio = 50
  VAL_CHANCE = 0.4  # 40% chance to be validation
  y_val = [B==i && idx !== nothing ? [0, idx[2]] : [0, i<B-1 && rand()<VAL_CHANCE ? val_ratio : T] for i in 1:B]
  o_data = Tuple([o_data, prices_C, prices_A, coin_value_inbtc, [y_val]])
  t_c = [y_val[i][2]==T ? ones(Float32, 1, M, 1) : zeros(Float32, 1, M, 1) for i in 1:B] # TODO finish stateconnet
  (Tuple([i_data]), o_data, ([t_c],)), coinmap
end
function data_from_dict(raw)
  # @show get_dims(raw.ins)
  time_dim = false
  if hasproperty(raw, :ins_shape) || hasproperty(raw, :outs_shape) ## crypto
    i_data = Float32.(raw.ins)
    i_data = hasproperty(raw, :ins_shape) ? reshape(i_data, raw.ins_shape...) : i_data  # TODO data shape is hardcoded 1,dims...
    i_data = i_data[:, :, 2:end, :]
    (i_data, o_data, t_c), _ = parse_crypto(i_data, split(raw.in_description[4], "_")[2:end])
    time_dim = true
  else
    i_data = rearrange_raw(raw.in_description, raw.ins)
    o_data = rearrange_raw(raw.out_description, raw.outs)
    data_batch_size = size(i_data[1], 1)
    if length(i_data[1]) > 0 && length(size(i_data[1])) > 3 && size(i_data[1],4) == 60 # DiabTrend Data
      time_size=size(i_data[1],4)
      i_data = Tuple([[[i_data[1, ..][i:i, ..,t] for i = 1:data_batch_size] for t = 1:time_size]])
      o_data = Tuple([[[o_data[1, ..][i:i, ..,t] for i = 1:data_batch_size] for t = 1:time_size]])
      time_dim = true
      t_c = (rearrange_raw(raw.time_connect_description, raw.state_connect),)
      # t_c = ([[t_c[1][1][i,j,..] for i in 1:size(t_c[1][1],1), j in 1:size(t_c[1][1],2)]],) # CORRECT BATCH handling
      t_c = ([[reshape(t_c[1][1][i,..], 1, :, 1) for i in 1:size(t_c[1][1],1)]],) # TODO why we need reshape?
    else
      i_data = Tuple([[[i_data[1, ..][i:i, ..]|>reshape(1, 1, :)  for i = 1:data_batch_size]]])
      o_data = Tuple([[[o_data[1, ..][i:i, ..]|>reshape(1, 1, :) for i = 1:data_batch_size]]])
      t_c = [[]]  # TODO move BATCH  based slicing 
    end
  end
  Data(t_c, i_data, o_data, time_dim)
end


end  # module DataWorld
