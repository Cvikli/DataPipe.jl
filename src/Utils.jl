
module Utils
let singleton = nothing
  global function static_data(data)
    if singleton === nothing 
      singleton = data
    else  
      singleton
    end
  end
end
static_data_fn(data) = () -> static_data(data)

get_dims(arr) = (length(size(arr[1])) > 0 ? (size(arr)..., get_dims(arr[1])...) : size(arr))
len(x) = length(x)
to_ranges(dims) = map(to -> 1:to, dims)

select(c, t, f) = c ? t : f

end 
